# Snake game

A simple snake game based on the original Snake arcade game.

### Installation

Pull or download repo and run index.html in your browser.

### Settings

Snake speed:
* Slow
* Medium  - set by default
* Fast

Walls:
* Yes - hitting a wall ends the game - set by default
* No - the snake can go trough walls

## Built With

* HTML5 Canvas
* Javascript