(function(){

	//Game variables

	//Canvas
	var canvas;
	var context;

	//Snake
	var snake_speed;
	var snake;
	var snake_direction;
	var snake_next_direction;

	//Food
	var food = {
		x: 0,
		y: 0
	};

	//Score
	var score;

	//Walls
	var walls;

	//HTML Elements
	var display_canvas;
	var display_menu;
	var display_gameover;
	var display_settings;

    var newgame_menu;
    var newgame_setting;
    var newgame_gameover;
    var settings_menu;
    var settings_gameover;
    var settings_speed;
    var settings_walls;
    var element_score;
    var end_score;


    var changeDirection = function(key){
        
        if(key == 38 && snake_direction != 2){
            snake_next_direction = 0;
        }else{
        
        if (key == 39 && snake_direction != 3){
            snake_next_direction = 1;
        }else{
        
        if (key == 40 && snake_direction != 0){
            snake_next_direction = 2;
        }else{
            
        if(key == 37 && snake_direction != 1){
            snake_next_direction = 3;
        } } } }
        
    }

    var drawDot = function(x, y) {
    	context.fillStyle = "#ff6d19";
    	context.fillRect(x * 10, y * 10, 10, 10);
    }

    var addFood = function() {
    	food.x = Math.floor(Math.random() * ((canvas.width / 10)));
    	food.y = Math.floor(Math.random() * ((canvas.height / 10)));
    	for(var i = 0; i < snake.length; i++) {
    		if(eatFood(food.x, food.y, snake[i].x, snake[i].y)){
    			addFood();
    		}
    	}
    }

    var eatFood = function(x, y, _x, _y) {
    	return (x == _x && y == _y) ? true : false;
    }

    //Main game function
    var mainGame = function() {

    	var _x = snake[0].x;
    	var _y = snake[0].y;
    	snake_direction = snake_next_direction;

    	//Up - 0, Right - 1, Down - 2, Left - 3
    	switch(snake_direction){
                case 0: _y--; break;
                case 1: _x++; break;
                case 2: _y++; break;
                case 3: _x--; break;
            }

    	snake.pop();
    	snake.unshift({x: _x, y: _y});

    	//Walls interaction
    	if (walls == 1) {
    		if (snake[0].x < 0 || snake[0].x == canvas.width / 10 || snake[0].y < 0 || snake[0].y == canvas.height / 10) {
    			end_score.innerHTML = score;
    			renderScreen("gameover");
    			return;
    		}
    	} else {
    		for (var i = 0, x = snake.length; i < x; i++) {
    			if(snake[i].x < 0) {
    				snake[i].x = snake[i].x + (canvas.width / 10);
    			}
    			if(snake[i].x == canvas.width / 10) {
    				snake[i].x = snake[i].x - (canvas.width / 10);
    			}
    			if(snake[i].y < 0) {
    				snake[i].y = snake[i].y + (canvas.height / 10);
    			}
    			if(snake[i].y == canvas.height / 10) {
    				snake[i].y = snake[i].y - (canvas.height / 10);
    			}
    		}
    	}

    	//Self hit
    	for (var i = 1; i < snake.length; i++) {
    		if(snake[0].x == snake[i].x && snake[0].y == snake[i].y){
    			end_score.innerHTML = score;
    			renderScreen("gameover");
    			return;
    		}
    	}

    	//Eating food
    	if(eatFood(snake[0].x, snake[0].y, food.x, food.y)) {
    		snake[snake.length] = {x: snake[0].x, y: snake[0].y};
    		score++;
    		setScore(score);
    		addFood();
    		drawDot(food.x, food.y);
    	}

    	context.beginPath();
    	context.fillStyle = "#333333";
    	context.fillRect(0, 0, canvas.width, canvas.height);

    	for(var i = 0; i < snake.length; i++) {
    		drawDot(snake[i].x, snake[i].y);
    	}

    	drawDot(food.x, food.y);
    
    	setTimeout(mainGame, snake_speed);
    }

    var setScore = function(value) {
    	element_score.innerHTML = value;
    }

    var newGame = function() {
    	renderScreen("canvas");
    	display_canvas.focus();

    	snake = [];
    	for(var i = 3; i >= 0; i--) {
    		snake.push({x: i, y: 15});
    	}

    	snake_next_direction = 1;

    	score = 0;
    	setScore(score);

    	addFood();

    	document.onkeydown = function(evt) {
            evt = evt || window.event;
            changeDirection(evt.keyCode);
        }

    	mainGame();

    }

    //Settings for game speed -- Lower value = faster
    var setSnakeSpeed = function(speed_value) {
    	snake_speed = speed_value;
    }

    var setWalls = function(walls_value) {
    	walls = walls_value;
    	if(walls == 0){
    		display_canvas.style.borderColor = "#ff6d19";
    		display_canvas.style.borderWidth = "2px";
    	}
    	if(walls == 1){
    		display_canvas.style.borderColor = "#ff6d19";
    	}
    }

    var renderScreen = function(screen){
    	switch(screen){

    		case "canvas":  display_canvas.style.display = "block";
    						display_menu.style.display = "none";
    						display_settings.style.display = "none";
    						display_gameover.style.display = "none";
    						break;

    		case "menu": 	display_canvas.style.display = "none";
    						display_menu.style.display = "block";
    						display_settings.style.display = "none";
    						display_gameover.style.display = "none";
    						break;

    		case "settings":display_canvas.style.display = "none";
    						display_menu.style.display = "none";
    						display_settings.style.display = "block";
    						display_gameover.style.display = "none";
    						break;

    		case "gameover":display_canvas.style.display = "none";
    						display_menu.style.display = "none";
    						display_settings.style.display = "none";
    						display_gameover.style.display = "block";
    						break;
    	}
    }


	window.onload = function(){

		canvas = document.getElementById("canvas");
		context = canvas.getContext("2d");

		display_canvas = document.getElementById("canvas");
		display_menu = document.getElementById("menu");
		display_gameover = document.getElementById('gameover');
		display_settings = document.getElementById('settings');

		newgame_menu = document.getElementById("newgame-menu");
		newgame_settings = document.getElementById("newgame-settings");
		newgame_gameover = document.getElementById("newgame-gameover");
		settings_menu = document.getElementById("settings-menu");
		settings_gameover = document.getElementById("settings-gameover");
		settings_speed = document.getElementsByName("speed");
		settings_walls = document.getElementsByName("walls");
		element_score = document.getElementById("score");
		end_score = document.getElementById("end-score");

		newgame_menu.onclick = function(){newGame();};
		newgame_settings.onclick = function(){newGame();};
		newgame_gameover.onclick = function(){newGame();};
		settings_menu.onclick = function(){renderScreen("settings")};
		settings_gameover.onclick = function(){renderScreen("settings")};

		setWalls(1);
		setSnakeSpeed(100);

		renderScreen("menu");

		for(var i = 0; i < settings_speed.length; i++) {
			settings_speed[i].addEventListener("click", function(){
				for(var i = 0; i < settings_speed.length; i++) {
					if(settings_speed[i].checked){
						setSnakeSpeed(settings_speed[i].value);
					}
				}
			});
		}

		for(var i = 0; i < settings_walls.length; i++) {
			settings_walls[i].addEventListener("click", function(){
				for(var i = 0; i < settings_walls.length; i++) {
					if(settings_walls[i].checked){
						setWalls(settings_walls[i].value);
					}
				}
			});
		}
	}
})();